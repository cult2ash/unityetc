Shader "Custom/AngleBlurShader" {
	Properties {
	 	_MainTex ("Texture", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
		_Dist ("Distance",Float) = 0.02
		_Angle ("Angle",Float) = 0
	}
	SubShader {
		Pass
        {
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
            
			struct adata 
			{
			   float4 vertex : POSITION;
			   float2 uv : TEXCOORD0;
			};

			struct v2f 
			{
			   float4 vertex : POSITION;
			   float2 uv : TEXCOORD0;
			};

			fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Dist;
			float _Angle;

			v2f vert( adata i )
			{
			   v2f o;

			   o.vertex = mul( UNITY_MATRIX_MVP, i.vertex);
			   o.uv = TRANSFORM_TEX(i.uv, _MainTex);

			   return o;
			}


			float4 frag(adata i) : SV_Target
			{   
				float2 uv=i.uv;
				uv.x+=0.1f;

                float4 c0= tex2D(_MainTex,i.uv+float2(_Dist*cos(_Angle),_Dist*sin(_Angle)));
                float4 c1= tex2D(_MainTex,i.uv+float2(_Dist*0.5f*cos(_Angle),_Dist*0.5f*sin(_Angle)));
			    float4 c2= tex2D(_MainTex,i.uv);
			    float4 c3= tex2D(_MainTex,i.uv+float2(-_Dist*0.5f*cos(_Angle),-_Dist*0.5f*sin(_Angle)));
			    float4 c4= tex2D(_MainTex,i.uv+float2(-_Dist*cos(_Angle),-_Dist*sin(_Angle)));

			    return c0*0.15f+c1*0.20f+c2*0.30f+c3*0.20f+c4*0.15f;
			}
			ENDCG
		}
	}
}
